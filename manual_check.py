from elasticsearch import Elasticsearch

es = Elasticsearch(['http://localhost:9200'])

res = es.search(index="tickets", body={ "query": 
                                            { "bool": 
                                              { "must": [
                                                {"term": {"ticket_type": ''}},
                                                {"term": {"ticket_number": ''}}
                                                ]
                                              }
                                            },
                                            "size": 100
                                          })

print(res)