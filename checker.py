import re
import json
import logging
from elasticsearch import Elasticsearch
from os import listdir
from os import path
import netaddr

logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s]  %(message)s', level = logging.WARN)

es = Elasticsearch(['http://localhost:9200'])
logs_dir = 'logs'


class Ticket:
    ticket = ''
    ticket_type = ''
    ticket_number = ''
    host_nets = netaddr.IPSet()
    dest_nets = netaddr.IPSet()
    es_host_nets = netaddr.IPSet()
    es_dest_nets = netaddr.IPSet()
    
    def __init__(self, ticket, ips_host, ips_dest):
        self.ticket = ticket
        self.ticket_type = str(ticket[:-12]).lower()
        self.ticket_number = ticket[-12:]

        for field in ips_host:
            if field[0]['parsed']:
                if 'ip' in field[0] and 'mask' in field[0]:
                    self.host_nets.add(netaddr.IPNetwork(field[0]['ip'] + '/' + field[0]['mask']))

        for field in ips_dest:
            if field[0]['parsed']:
                if 'ip' in field[0] and 'mask' in field[0]:
                    self.dest_nets.add(netaddr.IPNetwork(field[0]['ip'] + '/' + field[0]['mask']))

    def get_ticket_from_db(self, es):
        res = es.search(index="tickets", body={ "query": 
                                            { "bool": 
                                              { "must": [
                                                {"term": {"ticket_type": self.ticket_type}},
                                                {"term": {"ticket_number": self.ticket_number}}
                                                ]
                                              }
                                            },
                                            "size": 100
                                          })
        if res['hits']['total'] == 0:
            logging.warning('Ticket: ' + self.ticket + ' not found in es base.')
            return False
        for hit in res['hits']['hits']:
            self.es_host_nets.add(netaddr.IPNetwork(hit["_source"]["host_network"]))
            self.es_dest_nets.add(netaddr.IPNetwork(hit["_source"]["dst_network"]))
        return True

    def check_ticket(self, es):
        if self.get_ticket_from_db(es):
            if not self.host_nets.issubset(self.es_host_nets):
                logging.warning('Ticket: ' + self.ticket + ' NOT checked by host_nets.')
                return False
            if not self.dest_nets.issubset(self.es_dest_nets):
                logging.warning('Ticket: ' + self.ticket + ' NOT checked by dest_nets.')
                return False
            
            logging.warning('Ticket: ' + self.ticket + ' is checked.')
            return True
        return False

class TicketException(Exception):
    pass

if __name__ == "__main__":
    for log in listdir(logs_dir):
        log_file_path = path.join(logs_dir, log)
        fh = open(log_file_path, 'r')

        line = fh.readline()

        while line:
            try:
                if 'destinations' in line:
                    dest_raw = ''
                    line = fh.readline()
                    
                    while 'sources' not in line:
                        dest_raw += line
                        line = fh.readline()
                    
                    src_raw = ''
                    line = fh.readline()
                    while 'comment' not in line:
                        src_raw += line
                        line = fh.readline()
                    
                    ticket = str(fh.readline()).strip()
                    ticket_regex = re.compile('^[A-Z]{3}\d{12}$')
                    if not ticket_regex.match(ticket):
                        raise TicketException('Parse: bad ticket number')

                    ips_host_json = json.loads(src_raw.replace('\'', '"').replace('T','t').replace('F','f'))
                    ips_dest_json = json.loads(dest_raw.replace('\'', '"').replace('T','t').replace('F','f'))

                    #create ticket obj
                    t = Ticket(ticket, ips_host_json, ips_dest_json)
                    t.check_ticket(es)
            except TicketException as e:
                logging.warning(e)
            line = fh.readline()
